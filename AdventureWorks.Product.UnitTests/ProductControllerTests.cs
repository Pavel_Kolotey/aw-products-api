﻿using AdventureWorks.Product.Api.Controllers;
using AdventureWorks.Product.Api.Dal;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventureWorks.Product.UnitTests
{
    [TestFixture]
    public class ProductControllerTests
    {
        private Mock<IProductDao> _daoMock;
        private ProductController _controller;

        [SetUp]
        public void Setup()
        {
            _daoMock = new Mock<IProductDao>(MockBehavior.Strict);
            _controller = new ProductController(new Mock<ILogger<ProductController>>(MockBehavior.Loose).Object, _daoMock.Object);
        }

        [Test]
        public async Task List_CallsDao_ReturnsAll()
        {
            var list = new[]
            {
                new Api.Model.Product{ProductID = 1},
                new Api.Model.Product{ProductID = 2},
                new Api.Model.Product{ProductID = 5},
                new Api.Model.Product{ProductID = 10},
            };

            _daoMock.Setup(d => d.GetAll()).Returns(() => Task.FromResult<IEnumerable<Api.Model.Product>>(list));

            var result = await _controller.List();

            CollectionAssert.AreEqual(list, result);
        }

        [Test]
        public async Task Add_CallsDao_ReturnsId()
        {
            var model = new Api.Model.Product { };

            const int id = 1234;
            _daoMock.Setup(d => d.Insert(It.IsAny<Api.Model.Product>())).Returns(() => Task.FromResult<int>(id)).Verifiable();

            var result = await _controller.Add(model);

            Assert.AreEqual(id, result);
            _daoMock.Verify(d => d.Insert(model), Times.Once);
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public async Task Delete_CallsDao_TranslatesResult(bool dalResult)
        {
            const int id = 4654;

            _daoMock.Setup(d => d.Delete(It.IsIn<int>(id))).Returns(Task.FromResult(dalResult));

            var result = await _controller.Delete(id);

            Assert.AreEqual(dalResult, result);
        }
    }
}
