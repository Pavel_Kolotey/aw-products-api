using AdventureWorks.Product.Api.Dal;
using Microsoft.Extensions.Options;
using NUnit.Framework;

namespace AdventureWorks.Product.UnitTests
{
    [TestFixture]
    public class SqlConnectionFactoryTests
    {
        private DalConfig _config;

        private IOptionsSnapshot<DalConfig> _options;

        [SetUp]
        public void Setup()
        {
            _config = new DalConfig { ConnectionString = "Data Source=.;Integrated Security=yes;" };
            _options = new OptionsSnapshot<DalConfig>(_config);
        }

        [Test]
        public void Create_CreatesConnection_NotNullWithConfiguredConnectionString()
        {
            var factory = new SqlConnectionFactory(_options);

            var result = factory.Create();

            Assert.IsNotNull(result);
            Assert.AreEqual(_config.ConnectionString, result.ConnectionString);
        }

        private class OptionsSnapshot<T> : IOptionsSnapshot<T> where T : class
        {
            public OptionsSnapshot(T value)
            {
                Value = value;
            }

            public T Value { get; set; }

            public T Get(string name) => Value;
        }
    }
}