﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;

namespace AdventureWorks.Product.Api.Dal
{
    public class ProductDao : IProductDao
    {
        private readonly SqlConnectionFactory _connectionFactory;

        public ProductDao(SqlConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public async Task<IEnumerable<Model.Product>> GetAll()
        {
            await using var connection = _connectionFactory.Create();
            return await connection.QueryAsync<Model.Product>("SELECT * FROM Production.Product;");
        }

        public async Task<Model.Product> FindById(int id)
        {
            await using var connection = _connectionFactory.Create();
            return await connection.QueryFirstOrDefaultAsync<Model.Product>(
                "SELECT TOP 1 * FROM Production.Product WHERE ProductID = @id;", new { id = id });
        }

        public async Task<int> Insert(Model.Product product)
        {
            if (product == null) throw new ArgumentNullException(nameof(product));

            await using var connection = _connectionFactory.Create();
            return await connection.QueryFirstAsync<int>(@"
INSERT INTO [Production].[Product]
           ([Name]
           ,[ProductNumber]
           ,[MakeFlag]
           ,[FinishedGoodsFlag]
           ,[Color]
           ,[SafetyStockLevel]
           ,[ReorderPoint]
           ,[StandardCost]
           ,[ListPrice]
           ,[Size]
           ,[SizeUnitMeasureCode]
           ,[WeightUnitMeasureCode]
           ,[Weight]
           ,[DaysToManufacture]
           ,[ProductLine]
           ,[Class]
           ,[Style]
           ,[ProductSubcategoryID]
           ,[ProductModelID]
           ,[SellStartDate]
           ,[SellEndDate]
           ,[DiscontinuedDate]
           ,[rowguid]
           ,[ModifiedDate])
     VALUES
           (@Name
           ,@ProductNumber
           ,@MakeFlag
           ,@FinishedGoodsFlag
           ,@Color
           ,@SafetyStockLevel
           ,@ReorderPoint
           ,@StandardCost
           ,@ListPrice
           ,@Size
           ,@SizeUnitMeasureCode
           ,@WeightUnitMeasureCode
           ,@Weight
           ,@DaysToManufacture
           ,@ProductLine
           ,@Class
           ,@Style
           ,@ProductSubcategoryID
           ,@ProductModelID
           ,@SellStartDate
           ,@SellEndDate
           ,@DiscontinuedDate
           ,@rowguid
           ,@ModifiedDate);
           SELECT SCOPE_IDENTITY();", product);
        }

        public async Task<bool> Update(Model.Product product)
        {
            if (product == null) throw new ArgumentNullException(nameof(product));

            await using var connection = _connectionFactory.Create();
            var count = await connection.ExecuteAsync(@"
UPDATE [Production].[Product] SET
           [Name] = @Name,
           [ProductNumber] = @ProductNumber,
           [MakeFlag] = @MakeFlag,
           [FinishedGoodsFlag] = @FinishedGoodsFlag,
           [Color] = @Color,
           [SafetyStockLevel] = @SafetyStockLevel,
           [ReorderPoint] = @ReorderPoint,
           [StandardCost] = @StandardCost,
           [ListPrice] = @ListPrice,
           [Size] = @Size,
           [SizeUnitMeasureCode] = @SizeUnitMeasureCode,
           [WeightUnitMeasureCode] = @WeightUnitMeasureCode,
           [Weight] = @Weight,
           [DaysToManufacture] = @DaysToManufacture,
           [ProductLine] = @ProductLine,
           [Class] = @Class,
           [Style] = @Style,
           [ProductSubcategoryID] = @ProductSubcategoryID,
           [ProductModelID] = @ProductModelID,
           [SellStartDate] = @SellStartDate,
           [SellEndDate] = @SellEndDate,
           [DiscontinuedDate] = @DiscontinuedDate,
           [rowguid] = @rowguid,
           [ModifiedDate] = @ModifiedDate
           WHERE ProductID = @ProductId;", product);

            return count > 0;
        }

        public async Task<bool> Delete(int id)
        {
            await using var connection = _connectionFactory.Create();
            var count = await connection.ExecuteAsync(@"DELETE [Production].[Product] WHERE ProductID = @id;",
                new { id = id });
            return count > 0;
        }
    }
}