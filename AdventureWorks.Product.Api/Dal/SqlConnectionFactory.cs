﻿using System.Data.SqlClient;
using Microsoft.Extensions.Options;

namespace AdventureWorks.Product.Api.Dal
{
    public class SqlConnectionFactory
    {
        private readonly IOptionsSnapshot<DalConfig> _config;

        public SqlConnectionFactory(IOptionsSnapshot<DalConfig> config)
        {
            _config = config;
        }

        public SqlConnection Create()
        {
            return new(_config.Value.ConnectionString);
        }
    }
}