﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AdventureWorks.Product.Api.Dal
{
    public interface IProductDao
    {
        Task<bool> Delete(int id);
        Task<Model.Product> FindById(int id);
        Task<IEnumerable<Model.Product>> GetAll();
        Task<int> Insert(Model.Product product);
        Task<bool> Update(Model.Product product);
    }
}