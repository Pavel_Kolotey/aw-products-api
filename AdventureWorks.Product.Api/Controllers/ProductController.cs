﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using AdventureWorks.Product.Api.Dal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AdventureWorks.Product.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly ILogger<ProductController> _logger;
        private readonly IProductDao _productDao;

        public ProductController(ILogger<ProductController> logger, IProductDao productDao)
        {
            _logger = logger;
            _productDao = productDao;
        }

        [HttpGet]
        public async Task<IEnumerable<Model.Product>> List()
        {
            return await _productDao.GetAll();
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            var product = await _productDao.FindById(id);
            if (product == null) return NotFound();
            return Ok(product);
        }
        
        [HttpPost]
        public async Task<int> Add([Required]Model.Product product)
        {
            if (product.rowguid == Guid.Empty) product.rowguid = new Guid();
            product.ModifiedDate = DateTime.Now;
            return await _productDao.Insert(product);
        }
        
        [HttpPut]
        public async Task<bool> Update([Required]Model.Product product)
        {
            product.ModifiedDate = DateTime.Now;
            return await _productDao.Update(product);
        }
        
        [HttpDelete("{id}")]
        public async Task<bool> Delete(int id)
        {
            return await _productDao.Delete(id);
        }

    }
}